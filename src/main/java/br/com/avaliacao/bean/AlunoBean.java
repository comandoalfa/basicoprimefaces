package br.com.avaliacao.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.avaliacao.modelo.Aluno;
import br.com.avaliacao.modelo.EstadoCivil;
import br.com.avaliacao.modelo.Sexo;
import br.com.avaliacao.service.AlunoService;

@Named
@ViewScoped
public class AlunoBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Aluno aluno = new Aluno();
	private List<Aluno> alunos;
	private Integer alunoId;
	private Aluno alunoSelecionadoTabela;

	
	@Inject
	private AlunoService alunoService;
	
	public Sexo[] getSexos(){
		return Sexo.values();
	}
	
	public EstadoCivil[] getEstadoCivis() {
		return EstadoCivil.values();
	}
	
	
	public String salvar() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
			if (this.aluno.getId() == null) {
				aluno.setNome(this.aluno.getNome().toUpperCase());
				alunoService.salvar(aluno);
				this.alunos = alunoService.listar();
				context.getExternalContext().getFlash().setKeepMessages(true);
				context.addMessage(null, new FacesMessage("Aluno salvo com sucesso!"));
			} else {
				alunoService.atualizar(aluno);
				context.getExternalContext().getFlash().setKeepMessages(true);
				context.addMessage(null, new FacesMessage("Aluno atualizado com sucesso!"));
			}
		
		
		
		this.aluno = new Aluno();
		return "/templates/aluno/form?faces-redirect=true";
	}
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();
		alunoService.remover(alunoSelecionadoTabela);
		alunoSelecionadoTabela = null;
		this.alunos = alunoService.listar();
		context.addMessage(null, new FacesMessage("Aluno excluído com sucesso!"));
		
		
	}
	
	public void cancelarEdicao() {
		this.aluno = new Aluno();
		alunoSelecionadoTabela = null;
	}
	
	
	/*
	 * Carregar lista os alunos
	 */
	public List<Aluno> getAlunos(){
		
		if(this.alunos == null) {
			this.alunos = alunoService.listar();
		}
		return alunos;
	}



	public Aluno getAluno() {
		return aluno;
	}



	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public Integer getAlunoId() {
		return alunoId;
	}

	public void setAlunoId(Integer alunoId) {
		this.alunoId = alunoId;
	}

	public Aluno getAlunoSelecionadoTabela() {
		return alunoSelecionadoTabela;
	}

	public void setAlunoSelecionadoTabela(Aluno alunoSelecionadoTabela) {
		this.alunoSelecionadoTabela = alunoSelecionadoTabela;
		

	}

	
	
	

}
