package br.com.avaliacao.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.avaliacao.dao.AlunoDAO;
import br.com.avaliacao.dao.AvaliacaoDAO;
import br.com.avaliacao.dao.AvaliadorDAO;
import br.com.avaliacao.modelo.Aluno;
import br.com.avaliacao.modelo.Avaliacao;
import br.com.avaliacao.modelo.Avaliador;

@Named
@ViewScoped
public class AvaliacaoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Avaliacao avaliacao = new Avaliacao();
	private List<Avaliacao> avaliacoes;
	private Integer avaliacaoId;
	private Integer alunoId;
	private List<Aluno> alunos;
	private Avaliacao avaliacaoSelecionadaTabela;
	private List<Avaliador> avaliadores = new ArrayList<>();
	
	
	@Inject
	private AvaliacaoDAO avaliacaoDao;
	@Inject
	private AlunoDAO alunoDAO;
	@Inject
	private AvaliadorDAO avaliadorDao;
	
	public String salvar() {
		FacesContext context = FacesContext.getCurrentInstance();
		
		if(this.avaliacao.getId() == null) {
			
			avaliacaoDao.salvar(avaliacao);
			
			this.avaliacoes = avaliacaoDao.listar();
			context.getExternalContext().getFlash().setKeepMessages(true);
			context.addMessage(null, new FacesMessage("Avaliação salva com sucesso!"));
		} else {

			avaliacaoDao.atualizar(avaliacao);
			context.getExternalContext().getFlash().setKeepMessages(true);
			context.addMessage(null, new FacesMessage("Avaliação atualizada com sucesso!"));
		}
		
		this.avaliacao = new Avaliacao();
		return "/templates/avaliacao/tabela?faces-redirect=true";
	}
	
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();
		avaliacaoDao.remover(avaliacaoSelecionadaTabela);
		avaliacaoSelecionadaTabela = null;
		this.avaliacoes = avaliacaoDao.listar();
		context.addMessage(null, new FacesMessage("Avaliação excluída com sucesso!"));
		
		
	}
	
	
	public String cancelarEdicao() {
		this.avaliacao = new Avaliacao();
		avaliacaoSelecionadaTabela = null;
		this.alunoId = null;
		return "/templates/avaliacao/tabela?faces-redirect=true";
	}
	
	/*
	 * Implementação do autocomplete
	 */
	public List<Avaliador> autoCompleteAvaliador(String consultado){
		List<Avaliador> avaliadoresTemp = new ArrayList<>();
		for(Avaliador avaliador : this.avaliadores) {
			if(avaliador.getNome().toLowerCase().startsWith(consultado.toLowerCase())) {
				avaliadoresTemp.add(avaliador);
;			}
		}
		return avaliadoresTemp;
	}

	public Avaliacao getAvaliacao() {
		return avaliacao;
	}
	
	
	public void setAvaliacao(Avaliacao avaliacao) {
		this.avaliacao = avaliacao;
		
	}


	/*
	 * Carregar lista de avaliações
	 */
	public List<Avaliacao> getAvaliacoes() {
		
		if(this.avaliacoes == null) {
			this.avaliacoes = avaliacaoDao.listar();
		}
		return avaliacoes;
	}



	public Integer getAvaliacaoId() {
		return avaliacaoId;
	}



	public void setAvaliacaoId(Integer avaliacaoId) {
		this.avaliacaoId = avaliacaoId;
		
	}


	/*
	 * Retorna lista de Alunos
	 */
	public List<Aluno> getAlunos() {
		this.alunos = alunoDAO.listar();
		return alunos;
	}
	
	/*
	 * Retorna uma lista de Avaliadores
	 */
	public List<Avaliador> getAvaliadores() {
		this.avaliadores = avaliadorDao.listar();
		return avaliadores;
	}



	public Integer getAlunoId() {
		return alunoId;
	}



	public void setAlunoId(Integer alunoId) {
		this.alunoId = alunoId;
	}




	public Avaliacao getAvaliacaoSelecionadaTabela() {
		return avaliacaoSelecionadaTabela;
	}




	public void setAvaliacaoSelecionadaTabela(Avaliacao avaliacaoSelecionadaTabela) {
		this.avaliacaoSelecionadaTabela = avaliacaoSelecionadaTabela;
		System.out.println("Avaliacao selecionada " + avaliacaoSelecionadaTabela.getId());
		this.avaliacao = avaliacaoSelecionadaTabela;
		System.out.println("Valor atual da avaliacao" + this.avaliacao.getId());
		
	}


	public void prepararEdicao() {
		this.avaliacao = avaliacaoDao.buscarPorId(avaliacaoId);
	}
	




}
