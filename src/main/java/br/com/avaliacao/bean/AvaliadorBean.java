package br.com.avaliacao.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.avaliacao.dao.AvaliadorDAO;
import br.com.avaliacao.modelo.Avaliador;
import br.com.avaliacao.modelo.EstadoCivil;
import br.com.avaliacao.modelo.Sexo;

@Named
@ViewScoped
public class AvaliadorBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Avaliador avaliador = new Avaliador();
	private List<Avaliador> avaliadores;
	private Integer avaliadorId;
	private Avaliador avaliadorSelecionadoTabela;


	
	@Inject
	private AvaliadorDAO avaliadorDAO;
	
	public Sexo[] getSexos(){
		return Sexo.values();
	}
	
	public EstadoCivil[] getEstadoCivis() {
		return EstadoCivil.values();
	}
	
	
	public String salvar() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
			if (this.avaliador.getId() == null) {
				avaliador.setNome(this.avaliador.getNome().toUpperCase());
				avaliadorDAO.salvar(avaliador);
				this.avaliadores = avaliadorDAO.listar();
				context.getExternalContext().getFlash().setKeepMessages(true);
				context.addMessage(null, new FacesMessage("Avaliador salvo com sucesso!"));
			} else {
				avaliadorDAO.atualizar(avaliador);
				context.getExternalContext().getFlash().setKeepMessages(true);
				context.addMessage(null, new FacesMessage("Avaliador atualizado com sucesso!"));
			}
		
		
		
		this.avaliador = new Avaliador();
		return "/templates/avaliador/form?faces-redirect=true";
	}
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();
		avaliadorDAO.remover(avaliadorSelecionadoTabela);
		avaliadorSelecionadoTabela = null;
		this.avaliadores = avaliadorDAO.listar();
		context.addMessage(null, new FacesMessage("Avaliador excluído com sucesso!"));
		
		
	}
	
	public void cancelarEdicao() {
		this.avaliador = new Avaliador();
		avaliadorSelecionadoTabela = null;
	}
	
	
	/*
	 * Carregar lista os alunos
	 */
	public List<Avaliador> getAvaliadores(){
		
		if(this.avaliadores == null) {
			this.avaliadores = avaliadorDAO.listar();
		}
		return avaliadores;
	}



	public Avaliador getAvaliador() {
		return avaliador;
	}



	public void setAvaliador(Avaliador avaliador) {
		this.avaliador = avaliador;
	}

	public Integer getAvaliadorId() {
		return avaliadorId;
	}

	public void setAvaliadorId(Integer avaliadorId) {
		this.avaliadorId = avaliadorId;
	}

	public Avaliador getAvaliadorSelecionadoTabela() {
		return avaliadorSelecionadoTabela;
	}

	public void setAvaliadorSelecionadoTabela(Avaliador avaliadorSelecionadoTabela) {
		this.avaliadorSelecionadoTabela = avaliadorSelecionadoTabela;
		

	}

	
	
	

}
