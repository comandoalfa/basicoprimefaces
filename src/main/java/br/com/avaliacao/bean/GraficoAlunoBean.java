package br.com.avaliacao.bean;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;

@Named
@RequestScoped
public class GraficoAlunoBean {

	private LineChartModel model;
	
	public void preRender() {
		this.model = new LineChartModel();
		this.model.setAnimate(true);
		this.model.setTitle("Avaliações");
		this.model.setLegendPosition("e");
		
	}
	
	public void adicionarSerie(String rotulo) {
		ChartSeries series = new ChartSeries(rotulo);
		series.set("1", Math.random() * 200);
		series.set("2", Math.random() * 200);
		series.set("3", Math.random() * 200);
		series.set("4", Math.random() * 200);
		this.model.addSeries(series);
	}

	public CartesianChartModel getModel() {
		return model;
	}
	
}
