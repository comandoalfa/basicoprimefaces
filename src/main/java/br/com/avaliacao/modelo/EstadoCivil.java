package br.com.avaliacao.modelo;

public enum EstadoCivil {
	SOLTEIRO("Solteiro"),
	CASADO("Casado"),
	AMASIADO("Amasiado"),
	DIVORCIADO("Divorciado"),
	VIUVO("Viúvo");
	
	private String descricao;
	
	EstadoCivil(String descricao){
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
	

}
