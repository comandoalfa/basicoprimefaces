package br.com.avaliacao.modelo;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Avaliacao implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private Calendar data = Calendar.getInstance();
	
	@ManyToOne
	private Aluno aluno;
	
	@ManyToOne
	private Avaliador avaliador;
	
	private double torax;
	
	private double pescoco;
	
	private double cintura;
	
	private double quadril;
	
	private double bicepsDireito;
	
	private double bicepsEsquerdo;
	
	private double antebracoDireito;

	private double antebracoEsquerdo;
	
	private double coxaDireita;
	
	private double coxaEsquerda;
	
	private double panturrilhaDireita;
	
	private double panturrilhaEsquerda;
	
	private double peso;
	
	private double altura;
	
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Calendar getData() {
		return data;
	}
	public void setData(Calendar data) {
		this.data = data;
	}
	public Aluno getAluno() {
		return aluno;
	}
	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	public double getTorax() {
		return torax;
	}
	public void setTorax(double torax) {
		this.torax = torax;
	}
	public double getPescoco() {
		return pescoco;
	}
	public void setPescoco(double pescoco) {
		this.pescoco = pescoco;
	}
	public double getCintura() {
		return cintura;
	}
	public void setCintura(double cintura) {
		this.cintura = cintura;
	}
	public double getQuadril() {
		return quadril;
	}
	public void setQuadril(double quadril) {
		this.quadril = quadril;
	}
	public double getBicepsDireito() {
		return bicepsDireito;
	}
	public void setBicepsDireito(double bicepsDireito) {
		this.bicepsDireito = bicepsDireito;
	}
	public double getBicepsEsquerdo() {
		return bicepsEsquerdo;
	}
	public void setBicepsEsquerdo(double bicepsEsquerdo) {
		this.bicepsEsquerdo = bicepsEsquerdo;
	}
	public double getAntebracoDireito() {
		return antebracoDireito;
	}
	public void setAntebracoDireito(double antebracoDireito) {
		this.antebracoDireito = antebracoDireito;
	}
	public double getAntebracoEsquerdo() {
		return antebracoEsquerdo;
	}
	public void setAntebracoEsquerdo(double antebracoEsquerdo) {
		this.antebracoEsquerdo = antebracoEsquerdo;
	}
	public double getCoxaDireita() {
		return coxaDireita;
	}
	public void setCoxaDireita(double coxaDireita) {
		this.coxaDireita = coxaDireita;
	}
	public double getCoxaEsquerda() {
		return coxaEsquerda;
	}
	public void setCoxaEsquerda(double coxaEsquerda) {
		this.coxaEsquerda = coxaEsquerda;
	}
	public double getPanturrilhaDireita() {
		return panturrilhaDireita;
	}
	public void setPanturrilhaDireita(double panturrilhaDireita) {
		this.panturrilhaDireita = panturrilhaDireita;
	}
	public double getPanturrilhaEsquerda() {
		return panturrilhaEsquerda;
	}
	public void setPanturrilhaEsquerda(double panturrilhaEsquerda) {
		this.panturrilhaEsquerda = panturrilhaEsquerda;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public Avaliador getAvaliador() {
		return avaliador;
	}
	public void setAvaliador(Avaliador avaliador) {
		this.avaliador = avaliador;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Avaliacao other = (Avaliacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
