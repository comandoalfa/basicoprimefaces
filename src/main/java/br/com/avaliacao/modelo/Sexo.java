package br.com.avaliacao.modelo;

public enum Sexo {
	MASCUNINO("M"),
	FEMININO("F");
	
	private String descricao;
	
	Sexo(String descricao){
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
	
}
