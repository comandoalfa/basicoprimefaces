package br.com.avaliacao.modelo;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("AVALIADOR")
public class Avaliador extends Pessoa implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String numeroRegistro;
	

	public String getNumeroRegistro() {
		return numeroRegistro;
	}

	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}
	
	

}
