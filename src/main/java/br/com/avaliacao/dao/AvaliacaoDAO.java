package br.com.avaliacao.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.avaliacao.modelo.Avaliacao;

@Stateless
public class AvaliacaoDAO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext
	private EntityManager em;


	public void salvar(Avaliacao avaliacao) {
		em.persist(avaliacao);

	}

	public void remover(Avaliacao avaliacao) {
		em.remove(em.merge(avaliacao));
	}

	public void atualizar(Avaliacao avaliacao) {

		em.merge(avaliacao);
	}

	public List<Avaliacao> listar() {
		
		List<Avaliacao> lista = em.createQuery("from Avaliacao", Avaliacao.class).getResultList();
		return lista;
	}

	public Avaliacao buscarPorId(Integer id) {
		
		Avaliacao instancia = em.find(Avaliacao.class, id);
	
		return instancia;
	}


}
