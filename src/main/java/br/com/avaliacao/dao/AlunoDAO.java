package br.com.avaliacao.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.avaliacao.modelo.Aluno;

@Stateless
public class AlunoDAO {
	
	@PersistenceContext
	private EntityManager em;


	public void salvar(Aluno aluno) {
		em.persist(aluno);

	}

	public void remover(Aluno aluno) {
		em.remove(em.merge(aluno));
	}

	public void atualizar(Aluno aluno) {

		em.merge(aluno);
	}

	public List<Aluno> listar() {
		
		List<Aluno> lista = em.createQuery("from Aluno", Aluno.class).getResultList();
		return lista;
	}

	public Aluno buscarPorId(Integer id) {
		
		Aluno instancia = em.find(Aluno.class, id);
	
		return instancia;
	}


}
