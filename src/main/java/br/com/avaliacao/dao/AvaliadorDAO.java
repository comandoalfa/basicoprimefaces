package br.com.avaliacao.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.avaliacao.modelo.Avaliador;

@Stateless
public class AvaliadorDAO {
	
	@PersistenceContext
	private EntityManager em;


	public void salvar(Avaliador avaliador) {
		em.persist(avaliador);

	}

	public void remover(Avaliador avaliador) {
		em.remove(em.merge(avaliador));
	}

	public void atualizar(Avaliador avaliador) {

		em.merge(avaliador);
	}

	public List<Avaliador> listar() {
		
		List<Avaliador> lista = em.createQuery("from Avaliador", Avaliador.class).getResultList();
		return lista;
	}

	public Avaliador buscarPorId(Integer id) {
		
		Avaliador instancia = em.find(Avaliador.class, id);
	
		return instancia;
	}


}
