package br.com.avaliacao.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import br.com.avaliacao.dao.AvaliadorDAO;
import br.com.avaliacao.modelo.Avaliador;

@FacesConverter(forClass=Avaliador.class)
public class AvaliadorConverter implements Converter {
	
	@Inject
	private AvaliadorDAO avaliadorDao;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if(value != null && value.trim().length() > 0) {
			Integer id = Integer.valueOf(value);
			try {
				return avaliadorDao.buscarPorId(id);
			} catch (Exception e){
				throw new ConverterException("Não foi possível encontrar o Avaliador com esse id"
						+ value + e.getMessage());
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if(value != null) {
			Avaliador avaliador = (Avaliador)value;
			return avaliador.getId().toString();
		}
		return "";
	}

}
