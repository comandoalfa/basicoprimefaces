package br.com.avaliacao.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import br.com.avaliacao.dao.AlunoDAO;
import br.com.avaliacao.modelo.Aluno;

@FacesConverter(forClass=Aluno.class)
public class AlunoConverter implements Converter {
	
	@Inject
	private AlunoDAO alunoDao;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if(value != null && value.trim().length() > 0) {
			Integer id = Integer.valueOf(value);
			try {
				
				return alunoDao.buscarPorId(id);
			} catch (Exception e){
				throw new ConverterException("Não foi possível encontrar o Aluno com esse id"
						+ value + e.getMessage());
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if(value != null) {
			Aluno aluno = (Aluno)value;
			return aluno.getId().toString();
		}
		return "";
	}

}
