package br.com.avaliacao.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.avaliacao.dao.AlunoDAO;
import br.com.avaliacao.modelo.Aluno;

@Stateless
public class AlunoService {
	
	@Inject
	private AlunoDAO alunoDao;
	
	public void salvar(Aluno aluno) {

			alunoDao.salvar(aluno);
	}
	
	public void remover(Aluno aluno) {
		alunoDao.remover(aluno);
	}

	public void atualizar(Aluno aluno) {

		alunoDao.atualizar(aluno);
	}

	public List<Aluno> listar() {
		
		List<Aluno> lista = alunoDao.listar();
		return lista;
	}

	public Aluno buscarPorId(Integer id) {
		
		Aluno instancia = alunoDao.buscarPorId(id);
	
		return instancia;
	}

}
